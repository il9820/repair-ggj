﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSceneReset : MonoBehaviour
{
    public void Reset()
    {
        //Give the PlayerPrefs some values to send over to the next Scene
        PlayerPrefs.SetInt("Score", 0);
    }

}
