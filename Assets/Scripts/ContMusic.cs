﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContMusic : MonoBehaviour
{
    public AudioClip mainSong;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        PlayMainSong();
    }
    private static ContMusic instance = null;
    public static ContMusic Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void PlayMainSong()
    {
        audioSource.clip = mainSong;
    }
}