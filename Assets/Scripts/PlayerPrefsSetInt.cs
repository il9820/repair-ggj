﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsSetInt : MonoBehaviour
{
    public void BeatGame()
    {
        //Give the PlayerPrefs some values to send over to the next Scene
        PlayerPrefs.SetInt("Score", 1);
    }
}
