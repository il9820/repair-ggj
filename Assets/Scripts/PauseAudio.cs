﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseAudio : MonoBehaviour
{
    public AudioClip musicBoxSong;
    public AudioClip mainSong;
    private AudioSource audioSource;

    public void Pause()
    {
        audioSource = ContMusic.Instance.gameObject.GetComponent<AudioSource>();
        audioSource.Pause();
        audioSource.clip = musicBoxSong;
        audioSource.Play();
    }
    public void PlayMainSong()
    {
        audioSource.clip = mainSong;
        audioSource.Play();
    }
}
